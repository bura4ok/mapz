﻿using BenchmarkDotNet.Attributes;


namespace Lab2 {
    [MemoryDiagnoser]
    public class BenchMark {
        private int N = 1000000;

        private int intValue = 10;
        private object intBox = 10;

        [Benchmark]
        public void BoxingInt() {
            for (int i = 0; i < N; i++) {
                intBox = intValue;
            }
        }

        [Benchmark]
        public void UnboxingInt() {
            for (int i = 0; i < N; i++) {
                intValue = (int)intBox;
            }
        }

        int assignedInt = 0;

        [Benchmark]
        public void CommonAssignmentInt() {
            for (int i = 0; i < N; i++) {
                assignedInt = intValue;
            }
        }
        

        private double doubleValue = 10.5;
        private object doubleBox = 10.4;

        [Benchmark]
        public void BoxingDouble() {
            for (int i = 0; i < N; i++) {
                doubleBox = doubleValue;
                doubleValue = (double)doubleBox;
            }
        }

        [Benchmark]
        public void UnboxingDouble() {
            for (int i = 0; i < N; i++) {
                doubleValue = (double)doubleBox;
            }
        }

        double assignedDouble = 0;

        [Benchmark]
        public void CommonAssignmentDouble() {
            for (int i = 0; i < N; i++) {
                 assignedDouble = doubleValue;
            }
        }


        struct OneField {
            public double Value1;
        }

        struct TwoField {
            public double Value1;
            public double Value2;
        }

        struct FiveField {
            public double Value1;
            public double Value2;
            public double Value3;
            public double Value4;
            public double Value5;
        }


        OneField of = new OneField();
        object? boxStructure1;

        TwoField tf = new TwoField();
        object? boxStructure2;

        FiveField ff = new FiveField();
        object? boxStructure3;

        [Benchmark]
        public void OneFieldBox() {
            of.Value1 = 10.2;
            for (int i = 0; i < N; i++) {
                boxStructure1 = of; //boxing
            }
        }

        [Benchmark]
        public void OneFieldUnBox() {
            of.Value1 = 10.2;
            boxStructure1 = of; //boxing треба разок 
            for (int i = 0; i < N; i++) {
                of = (OneField)boxStructure1; // unboxing
            }
        }


        [Benchmark]
        public void TwoFieldBox() {
            tf.Value1 = 10.3;
            tf.Value2 = 20.3;
            for (int i = 0; i < N; i++) {
                boxStructure2 = tf; //boxing
            }
        }
        [Benchmark]
        public void TwoFieldUnBox() {
            tf.Value1 = 10.3;
            tf.Value2 = 20.3;
            boxStructure2 = tf; //boxing
            for (int i = 0; i < N; i++) {
                tf = (TwoField)boxStructure2; // unboxing
            }
        }

        [Benchmark] 
        public void FiveFieldBox() {
            ff.Value1 = 10.3;
            ff.Value2 = 20.3;
            ff.Value3 = 30.3;
            ff.Value4 = 40.3;
            ff.Value5 = 50.3;
            for (int i = 0; i < N; i++) {
                boxStructure3 = ff; //boxing
            }
        }

        [Benchmark]
        public void FiveFieldUnBox() {
            ff.Value1 = 10.3;
            ff.Value2 = 20.3;
            ff.Value3 = 30.3;
            ff.Value4 = 40.3;
            ff.Value5 = 50.3;
            boxStructure3 = ff; //boxing
            for (int i = 0; i < N; i++) {
                ff = (FiveField)boxStructure3; // unboxing
            }
        }
    } 
}

