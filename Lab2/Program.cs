﻿//Реалізувати ланцюжок наслідування у якому б був звичайний клас, абстрактний
//клас та інтерфейс. Перелічіть відмінності та подібності у цих структурах у звіті у
//вигляді таблиці.

using BenchmarkDotNet.Running;

interface ICar {
    void OpenCar();
}

abstract class Car {
    protected string Brand;
    protected string Model;

    protected Car(string brand, string model) {
        Brand = brand;  
        Model = model;
    }

    public abstract void StartEngine();


}

internal class Truck : Car, ICar { // множинне наслідування 


    //Оголошення внутрішнього класу 

    public class Trunk { 

    }

    public Trunk trunk; // щоб помилки не було


    private int NumOfWheels;
    private string Fuel;

    public Truck(string brand, string model, int wheels, string fuel) : base(brand, model) {
        this.NumOfWheels = wheels;
        this.Fuel = fuel;
    }

    public Truck(Trunk trunk, int numOfWheels, string fuel) : base("a", "b"){
        this.trunk = trunk;
        NumOfWheels = numOfWheels;
        Fuel = fuel;
    }

    public Truck() : this("M-B", "X", 4, "petrol") { }

    public void OpenWindow() {
        Console.WriteLine("Opening all windows");
    }
    public void OpenWindow(int window) {
        Console.WriteLine("Opening {0} window", window);
    }

    void ICar.OpenCar() {
        Console.WriteLine("Opening truck with card-key");
    }

    public override void StartEngine() {
        Console.WriteLine("Supplying fuel into engine...\nEngine is started");
    }

    public void ShowDetails() {
        Console.WriteLine("Brand: {0}\nModel: {1}\nWheels: {2}\nFuel: {3}",Brand, Model, NumOfWheels, Fuel);
    }

    // implicit explicit

    public static implicit operator int(Truck truck) { //неявне 
        return truck.NumOfWheels;
    }

    public static explicit operator string(Truck truck) => truck.Fuel; //явне


    // перевантаження класу object 

    public override bool Equals(object? obj) {
        if (obj == null || GetType() != obj.GetType())
            return false;

        Truck other = (Truck)obj;
        return Brand == other.Brand && Model == other.Model &&
               NumOfWheels == other.NumOfWheels && Fuel == other.Fuel;
    }

    public override int GetHashCode() {
        return HashCode.Combine(Brand, Model, NumOfWheels, Fuel);
    }
}

//ctrl + h class -> struct

//interface ICar1 {
//    void OpenCar();
//}

//abstract struct Car1 {
//    protected string Brand;
//    protected string Model;

//    public Car1(string brand, string model) {
//        Brand = brand;
//        Model = model;
//    }

//    public abstract void StartEngine();


//}

//internal struct Truck1: Car1, ICar1 {  

//    private int NumOfWheels;
//    private string Fuel;

//    public Truck(string brand, string model, int wheels, string fuel) : base(brand, model) { 
//    }

//    public Truck(Trunk trunk, int numOfWheels, string fuel) : base("a", "b") {
        
//    }

//    public void OpenCar() {
//    }
//}

// визначення дефолтних модифікаторів

class A {               // internal
    A() { }             // private
    int num;            // private 
    void Foo() {  }     // private 

    class Bar { }       //private
}

struct B {              //internal
    int num;            // private 
    void Foo() { }      // private 
}

interface IA {          // internal
    void Foo() { }      // public 
}


// enum 

enum CarColor {
   White = 1, 
   Black = 2, 
   Yellow = 4, 
   Blue = 5,
   Green = Yellow | Blue, //через оператор or виконується "злиття двох кольорів"
   NotGreen = ~Green,     // ~інвертує всі значення бітів 
   OrangeAndBlue = 7,
   Orange = OrangeAndBlue ^ Blue, //^ використовуємо для виключення бітів "синього кольору"
   Invisible = 0,
}


//різні види ініціалізації 

class Initialization {
    private int a = 10; // ініціалізація при кожному створенні екземпляра класу 
    private static int b = 10; // ініціалізація лише один раз при завантаженні класу у пам'ять

    public Initialization() { // ініціалізація через конструктор 
        Console.WriteLine("Dynamic");

    }   
    
    static Initialization() { // не може мати параметрів
        Console.WriteLine("Static");
        b = 10;
    }
}

class Initialization2 : Initialization {


    public Initialization2()  {
        Console.WriteLine("Dynamic child");
    }

    static Initialization2() { Console.WriteLine("Static child"); }
}


//наслідування у структур через інтерфейси
struct Auto : ICar {
    private string model;

    public void OpenCar() {
        Console.WriteLine("Open car");
    }
}



namespace Lab2 {
     class Program {
        static void Main(String[] args) {


            var summary = BenchmarkRunner.Run<BenchMark>();




            //ObjectMethodsTest();

            //ImplicitExplicitTest();

            //BoxUnboxTest();

            //OutAndRefTest();

            //Initialization2 obj = new Initialization2();

            //CarTest();

            //EnumTest();

        }

       


        static void ObjectMethodsTest() {
            Truck truck = new Truck();
            Truck truck1 = new Truck("Vovlo", "Fh16", 6, "Petrol");
            bool a = truck.Equals(truck1);
        }

        static void ImplicitExplicitTest() {
            Truck truck = new Truck();

            string fuel = (string)truck; //explicit потребує явного типу для перетворення 
            int wheels = truck; // implicit не потребує його 

        }


        static void BoxUnboxTest() {
            int a = 10;
            object b = a; // boxing, тобто поміщення int у кучу та отримання посилання на цю ділянку пам'яті
            Console.WriteLine("Boxed:" + b);


            b = 20;
            int c = (int)b; //unboxing
            Console.WriteLine("Unboxed:" + c);
        }

        static void OutAndRefTest() {
            int a = 1;
            Change(a);
            Console.WriteLine("without ref and out: a = " + a);

            int b = 1;
            ChangeRef(ref b); // вимагає ініціалізацію і не вимагає ініціалізацію всередині
            Console.WriteLine("ref: a = " + b);

            int c; // не вимагає попередньої ініціалізації як ref, але вимагає ініціалізацію всередині 
            ChangeOut(out c);
            Console.WriteLine("out: a = " + c);
        }

        static void Change(int num) {
            num = 10;
            
        }
        static void ChangeRef(ref int num) {
            num = 10;
            //Console.WriteLine(num + 10);
        }
        static void ChangeOut(out int num) {
            num = 10;
        }

        static void CarTest() {
            //Truck MyTruck = new Truck("Volvo", "Iron Knight", 4, "Diesel");
            Truck MyTruck = new Truck();

            ((ICar)MyTruck).OpenCar();

            MyTruck.StartEngine();
            MyTruck.ShowDetails();

            MyTruck.OpenWindow();
            MyTruck.OpenWindow(2);

            //MyTruck.Brand = "0"; // то для модифікаторів доступу
        }

        static void EnumTest() {
            //через оператор & ми перевіряємо наявність певних бітів, тут білий = 01,
            //а чорний = 10, тому результат буде 00,
            CarColor color = CarColor.White & CarColor.Black; 
            Console.WriteLine(color);

            if (color == 0 && CarColor.Green != 0) {
                Console.WriteLine("Invisible and green");
            } else {
                Console.WriteLine("Visible");
            }
        }
    }
}