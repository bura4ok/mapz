﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Classes {

    public class Hotel {
        private string name;

        public string Name { 
            get { return name; }
            set { name = value; }
        }

        private List<Room> rooms = new List<Room>();

        public void Place(IFurniture furn, int numberOfRoom) {
            rooms[numberOfRoom].Place(furn);
        }

        private class Room {
            private RoomTypes RoomType;
            private int Number;
            private List<IFurniture> furnitures
                = new List<IFurniture>();


            public Room(RoomTypes type, int number) {
                RoomType = type;
                Number = number;
            }

            public void Place(IFurniture furn) {
                this.furnitures.Add(furn);
                Console.WriteLine($"Placed {furn.GetType().Name} in room number {Number}");
            }

            public override string ToString() {
                var sb = new StringBuilder();
                sb.Append(RoomType.ToString()).Append(" ");
                sb.Append(Number.ToString()).Append(" ");
                foreach (var a in furnitures) {
                    sb.Append(a.ToString()).Append(" ");
                }
                return sb.Append("|").ToString();
            }

        }

        public void AddRoom(RoomTypes type) {
            rooms.Add(new Room(type, rooms.Count + 1));  // тутво одиниця так шо ну , рахуй не з нуля!!!!
        }

        public string GetAllInfo() {
            var sb = new StringBuilder();
            foreach(var room in rooms) { 
                sb.Append(room.ToString());
            }
            return sb.ToString();
            
        }

        public override string ToString() {
            var sb = new StringBuilder();
            sb.AppendLine($"The hotel \"{Name}\" has next rooms:");
            foreach (var room in rooms) {
                sb.AppendLine(room.ToString());
            }
            sb.AppendLine();
            return sb.ToString();
        }

    }

    public interface ISpecifyHotelName {
        public ISpecifyRoomTypeAndBuild WithName(string name);
    }

    public interface ISpecifyRoomTypeAndBuild {
        public ISpecifyRoomTypeAndBuild WithRoomType(RoomTypes roomType);
        public Hotel Build(); 
    }


    public class HotelBuilder {

        public static ISpecifyHotelName CreateHotel() {
            return new Creator();
        }

        private class Creator :
            ISpecifyHotelName,
            ISpecifyRoomTypeAndBuild {

            private Hotel hotel = new Hotel();


            public ISpecifyRoomTypeAndBuild WithName(string name) {
                hotel.Name = name;
                return this;
            }

            public ISpecifyRoomTypeAndBuild WithRoomType(RoomTypes roomType) {
                hotel.AddRoom(roomType);
                return this;
            }

            public Hotel Build() {
                return hotel;
            }
        }
    }

    public enum RoomTypes {
        Economy,
        Standard,
        Delux
    }
}
