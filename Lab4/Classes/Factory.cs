﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Classes {
    public interface IFurniture {
        void Show();
    }

    internal class Table : IFurniture {
        public void Show() {
            Console.WriteLine("The table was placed in room");
        }

        public override string ToString() {
            return "Table";
        }


    }

    internal class Mirror : IFurniture {
        public void Show() {
            Console.WriteLine("Mirror was placed in room, enjoy");
        }
        public override string ToString() {
            return "Mirror";
        }

    }

    internal class Bed : IFurniture {
        public void Show() {
            Console.WriteLine("Door was placed");
        }
        public override string ToString() {
            return "Bed";
        }


    }

    public interface IFurnitureFactory {
        IFurniture Create();
    }

    internal class TableFactory : IFurnitureFactory {
        public IFurniture Create() {
            Console.WriteLine("Table was created");
            return new Table();
        }
    }

    internal class BedFactory : IFurnitureFactory {
        public IFurniture Create() {
            Console.WriteLine("Bed was created");
            return new Bed();
        }
    }

    internal class MirrorFactory : IFurnitureFactory {
        public IFurniture Create() {
            Console.WriteLine("Mirror was created");
            return new Mirror();
        }
    }

    public class FurnitureFactory {
        private List<Tuple<string, IFurnitureFactory>> factories =
            new List<Tuple<string, IFurnitureFactory>>();

        public FurnitureFactory() {
            foreach (var t in typeof(FurnitureFactory).Assembly.GetTypes()) {
                if (typeof(IFurnitureFactory).IsAssignableFrom(t) && !t.IsInterface) {
                    factories.Add(Tuple.Create(
                        t.Name.Replace("Factory", string.Empty),
                        (IFurnitureFactory)Activator.CreateInstance(t)));
                }
            }
        }

        public IFurniture MakeFurniture(int id) {
            for (var index = 0; index < factories.Count; index++) {
                var tuple = factories[index];
                Console.WriteLine($"{index}: {tuple.Item1}");
            }
            return factories[id].Item2.Create();
        }
    }
}
