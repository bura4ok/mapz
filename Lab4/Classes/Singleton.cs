﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Classes {


    public sealed class ClientFlow {

        private static ClientFlow instance;
        public static int clientCount;

        private ClientFlow() {
            clientCount = 0;
        }


        public static ClientFlow Instance {
            get {
                if (instance == null) {
                    instance = new ClientFlow();
                }
                return instance;
            }
        }

        public void IncreaseClients(int amount) {
            clientCount += amount;
        }

        public void DecreaseClients(int amount) {
            clientCount -= amount;
        }

        public int Clients {
            get { return clientCount; }
        }
    }
}
