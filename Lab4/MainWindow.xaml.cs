﻿using Lab4.Classes;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace Lab4 {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public const string ROOMPATH = "D:\\UNIVERSITY\\Course 2\\Term 4\\MAPZ\\mapz\\Lab4\\Images\\room.png";
        public const string BEDPATH = "D:\\UNIVERSITY\\Course 2\\Term 4\\MAPZ\\mapz\\Lab4\\Images\\Bed.png";
        public const string MIRRORPATH = "D:\\UNIVERSITY\\Course 2\\Term 4\\MAPZ\\mapz\\Lab4\\Images\\Mirror.png";
        public const string CHAIRPATH = "D:\\UNIVERSITY\\Course 2\\Term 4\\MAPZ\\mapz\\Lab4\\Images\\Chair.png";
        public const string BUTTONPATH = "D:\\UNIVERSITY\\Course 2\\Term 4\\MAPZ\\mapz\\Lab4\\Images\\Button.png";

        public MainWindow() {
            InitializeComponent();
        }

        public Model model = new Model();
        public FurnitureFactory factory = new FurnitureFactory();
        private static int num = 0;

        //Main model
        public class Model {

            private ISpecifyRoomTypeAndBuild hotelWithName;
            private Hotel hotel;

            public Hotel Hotel {
                get { return hotel; } 
            }

            public void SetName(string name) {
                hotelWithName =  HotelBuilder.CreateHotel().WithName(name);
            }

            public void SetRoom(RoomTypes type) {

                hotelWithName.WithRoomType(type);
            }

            public void Create() {
                hotel = hotelWithName.Build();
            }
        }


        //buttons
        private void btn_setname_Click(object sender, RoutedEventArgs e) {
            model.SetName(tb_name.Text);
            MessageBox.Show("Created new name", "Success", MessageBoxButton.OK, MessageBoxImage.Information);

        }
        private void btn_setroom_Click(object sender, RoutedEventArgs e) {
            if (num > 4) {
                MessageBox.Show("Too many rooms", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            model.SetRoom(ParseRoomType(cb_type.Text));
            ClientFlow clients = ClientFlow.Instance;

            clients.IncreaseClients(1);

            MessageBox.Show("Added new room", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            num++;
        }
        private void btn_create_Click_1(object sender, RoutedEventArgs e) {
            if(model.Hotel == null) {
                model.Create();
                DrawHotel();
                MessageBox.Show("Created new hotel", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            } else {
                canvas.Children.Clear();
                DrawHotel();
            } 
        }
        private void btn_addfurn_Click(object sender, RoutedEventArgs e) {
            if (model.Hotel == null) {
                MessageBox.Show("There is no hotel", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IFurniture item = factory.MakeFurniture(ParseFurniture(cb_furn.Text));

            if (int.Parse(tb_roomNumber.Text) > num || int.Parse(tb_roomNumber.Text) < 0) {
                MessageBox.Show("Wrong number", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            model.Hotel.Place(item, int.Parse(tb_roomNumber.Text) - 1);


            ClientFlow clients = ClientFlow.Instance;
            clients.IncreaseClients(1);

            canvas.Children.Clear();
            DrawHotel();
            MessageBox.Show("Added new Furniture", "Success", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        //rendering
        public void DrawHotel() {
            ClientFlow clients = ClientFlow.Instance;

            DrawLabel(model.Hotel.Name, new Point(10, 10), 30);
            DrawLabel(clients.Clients.ToString(), new Point (700, 10), 30);

            var info = model.Hotel.GetAllInfo();

            int id = 0;
            double x = 10, y = 50;

            string[] parts = info.Split('|');

            foreach(var part in parts) {
                if (part == String.Empty) {
                    continue;
                }

                DrawPhoto(ROOMPATH, new Point(x + id, y));
                string[] parsedInfo = part.Split(' ');

                DrawLabel(parsedInfo[0], new Point(x + 40 + id, y + 130), 16);
                DrawLabel(parsedInfo[1], new Point(x + 68 + id, y + 21), 12);

                for(int i = 2; i <= parsedInfo.Length - 1; i++) {
                    if (parsedInfo[i] == String.Empty) {
                        continue;
                    }
                    DrawFurniture(parsedInfo[i], x + id ,y);
                }

                id += 140;
            }
        }
        private void DrawFurniture(string furn, double x, double y) {

            switch(furn.ToLower()) {
                case "bed":
                    DrawPhoto(BEDPATH, new Point(x + 1, y + 50));
                    return;
                case "table":
                    DrawPhoto(CHAIRPATH, new Point(x + 45, y + 75));
                    return;
                case "mirror":
                    DrawPhoto(MIRRORPATH, new Point(x + 30, y + 17));
                    return;
                default:
                    throw new ArgumentException("Invalid room type string.", nameof(furn));
            }
        }
        public void DrawPhoto(string path, Point point) {
            BitmapImage bitmap = new BitmapImage(new Uri(path));
            Image image = new Image();
            image.Source = bitmap;
            image.Width = bitmap.Width / 2.2;
            image.Height = bitmap.Height / 2.2;
            Canvas.SetLeft(image, point.X);
            Canvas.SetTop(image, point.Y);
            canvas.Children.Add(image);
        }
        public void DrawLabel(string text, Point point, int font) {
            TextBlock textBlock = new TextBlock();
            textBlock.Text = text;
            textBlock.FontSize = font;

            Canvas.SetLeft(textBlock, point.X);
            Canvas.SetTop(textBlock, point.Y);

            canvas.Children.Add(textBlock);
        }

        //parsing
        public int ParseFurniture(string furn) {
            switch (furn.ToLower()) {
                case "bed":
                    return 1;
                case "mirror":
                    return 2;
                case "table":
                    return 0;
                default:
                    throw new ArgumentException("Invalid room type string.", nameof(furn));
            }
        }

        public RoomTypes ParseRoomType(string roomTypeString) {
            switch (roomTypeString.ToLower()) {
                case "economy":
                    return RoomTypes.Economy;
                case "standard":
                    return RoomTypes.Standard;
                case "deluxe":
                    return RoomTypes.Delux;
                default:
                    throw new ArgumentException("Invalid room type string.", nameof(roomTypeString));
            }
        }

    }
}
