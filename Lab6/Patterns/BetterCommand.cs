﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Patterns {
    public interface ICommand {
        void Execute(); 
    }

    public class SaveHotelCommand : ICommand {
        Hotel _hotel;

        public SaveHotelCommand(Hotel hotel)
        {
            _hotel = hotel;
        }

        public void Execute() {
            _hotel.Save();
        }
    }

    public class ClearHotelCommand : ICommand {
        Hotel _hotel;

        public ClearHotelCommand(Hotel hotel) {
            _hotel = hotel;
        }

        public void Execute() {
            _hotel.Clear();
        }
    }
}
