﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lab6.Patterns
{
    public class Hotel
    {
        public IHotelState _state = new EmptyState();
        public double moneyBoost;
        public string GetState => _state.GetState();

        public void TransitTo(IHotelState state) {
            _state = state;
            _state.SetHotel(this);
        }

        public void InitHotel() {
            _state.Inited();
        }

        public void FurnishedHotel() {
            _state.Furnished();
        }

        EventManager _eventManager;
        PlayerProfile _profile;

        public Hotel() {
            _eventManager = new EventManager();
            _profile = PlayerProfile.Profile;
            _eventManager.Subscribe(_profile);
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int RoomCount => rooms.Count;

        private List<Room> rooms = new List<Room>();

        public void Place(IComponent furn, int numberOfRoom)
        {
            rooms[numberOfRoom].Place(furn);

        }

        public void Clear() {
            foreach(Room room in rooms) {
                room.Clear();
            }
            rooms.Clear();
           
            MessageBox.Show("Hotel was cleared", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public List<RoomTypes> RoomTypes
        {
            get
            {
                List<RoomTypes> types = new List<RoomTypes>();
                foreach (Room room in rooms)
                {
                    types.Add(room.RoomType);
                }
                return types;
            }
        }

        private class Room
        {
            private RoomTypes _roomType;
            private int Number;

            public RoomTypes RoomType
            {
                get { return _roomType; }
            }

            private Composite? furniture = new Composite();

            public void Clear() {
                furniture = new Composite();
            }

            public Room(RoomTypes type, int number)
            {
                _roomType = type;
                Number = number;
            }

            public void Place(IComponent furn)
            {
                furniture.AddComponent(furn);
            }

            public override string ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine($"Room number {Number} is the {_roomType} room with: ");
                if (furniture == null) {
                    sb.Append("Empty");
                    return sb.ToString();
                }
                sb.Append(furniture.ToString());
                return sb.ToString();
            }
        }

        public void AddRoom(RoomTypes type)
        {
            rooms.Add(new Room(type, rooms.Count + 1));
            _eventManager.Notify(1);

        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"The hotel \"{Name}\" has next rooms:");
            foreach (var room in rooms)
            {
                sb.AppendLine(room.ToString());
            }
            sb.AppendLine();
            return sb.ToString();
        }

        public void Save() {
            MessageBox.Show("Hotel was saved", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
    #region a
    public interface ISpecifyHotelName
    {
        public ISpecifyRoomTypeAndBuild WithName(string name);
    }

    public interface ISpecifyRoomTypeAndBuild
    {
        public ISpecifyRoomTypeAndBuild WithRoomType(RoomTypes roomType);
        public Hotel Build();
    }


    public class HotelBuilder
    {

        public static ISpecifyHotelName CreateHotel()
        {
            return new Creator();
        }

        private class Creator :
            ISpecifyHotelName,
            ISpecifyRoomTypeAndBuild
        {

            private Hotel hotel = new Hotel();


            public ISpecifyRoomTypeAndBuild WithName(string name)
            {
                hotel.Name = name;
                return this;
            }

            public ISpecifyRoomTypeAndBuild WithRoomType(RoomTypes roomType)
            {
                hotel.AddRoom(roomType);
                return this;
            }

            public Hotel Build()
            {
                hotel._state.SetHotel(hotel);
                return hotel;
            }
        }
    }

    public enum RoomTypes
    {
        Economy,
        Standart,
        Delux
    }
    #endregion
}
