﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Patterns
{
    public sealed class PlayerProfile : IListener
    {
        private static PlayerProfile? _profile;

        private PlayerState _state;

        private double _money = 10000;
        private int _level;
        private int numOfFurniture;

        public int Furniture {
            get { return numOfFurniture; }
        }


        private PlayerProfile()
        {
            _state = PlayerState.Owner;
            _money = 0;
            _level = 1;
        }

        public static PlayerProfile Profile
        {
            get
            {   
                if (_profile == null) {
                    _profile = new PlayerProfile();
                }
                return _profile;
            }
        }

        public PlayerState State {
            get { return _state; }
        }

        public double Money
        {
            get { return _money; }
        }

        public int Level
        {
            get { return _level; }
            set { _level = value; }
        }

        public void ChangeState() {
            if (_state == PlayerState.Owner) {
                _state = PlayerState.Spectator;
            }
            else if (_state == PlayerState.Spectator) {
                _state = PlayerState.Owner;
            }
        }

        public void Update(int num) {
            numOfFurniture += num;
        }
    }

    public enum PlayerState
    {
        Spectator,
        Owner
    }
}
