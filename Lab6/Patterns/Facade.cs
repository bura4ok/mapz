﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Patterns
{
    public class HotelFacade : IHotelFacade {

        private Hotel _hotel;

        public Hotel Hotel {
            get { return _hotel; }
        }

        public void TransitTo(IHotelState state) {
            _hotel.TransitTo(state);
        }


        

        public void InitHotel() {
            _hotel.InitHotel();
        }

        public void FurnishHotel() {
            _hotel.FurnishedHotel();
        }

        public void Clear() {
            _hotel.Clear();
        }

        
        public void AddFurnitureToAll(IComponent item) {
            for (int i = 0; i < _hotel.RoomCount; i++) {
                _hotel.Place(item, i);
            }

        }
        public void CreateHotelSet1(string name) {
            _hotel = HotelBuilder.CreateHotel()
               .WithName(name)
               .WithRoomType(RoomTypes.Standart)
               .WithRoomType(RoomTypes.Economy)
               .WithRoomType(RoomTypes.Standart)
               .Build();
        }
        public void CreateHotelSet2(string name) {
            _hotel = HotelBuilder.CreateHotel()
              .WithName(name)
              .WithRoomType(RoomTypes.Standart)
              .WithRoomType(RoomTypes.Standart)
              .WithRoomType(RoomTypes.Economy)
              .WithRoomType(RoomTypes.Delux)
              .Build();
        }
        public override string ToString() {
            return _hotel.ToString();
        }
        public void CreateFurnitureTest() {
           
            var roomTypes = _hotel.RoomTypes;
            int i = 0;
            foreach (var roomType in roomTypes) {
                switch (roomType) {
                    case RoomTypes.Standart:
                        _hotel.Place(CreateStandardRoom(), i);
                        break;
                    case RoomTypes.Economy:
                        _hotel.Place(CreateEconomyRoom(), i);
                        break;
                    case RoomTypes.Delux:
                        _hotel.Place(CreateDeluxeRoom(), i);
                        break;
                    default:
                        throw new ArgumentException();
                }
                i++;
            }
        }

        private IComponent CreateStandardRoom() {
            var bedroom = new Composite("Bedroom");
            bedroom.AddComponent(new Bed());
            bedroom.AddComponent(new Chair());

            var corridor = new Composite("Corridor");
            corridor.AddComponent(new Mirror());

            var room = new Composite("Apartments");
            room.AddComponent(bedroom);
            room.AddComponent(corridor);
            return room;
        }
        private IComponent CreateEconomyRoom() {
            var room = new Composite("Apartments");
            room.AddComponent(new Bed());
            room.AddComponent(new Chair());
            room.AddComponent(new Mirror());
            return room;
        }

        private IComponent CreateDeluxeRoom() { 
            var firstBedroom = new Composite("First Bedroom");
            firstBedroom.AddComponent(new Bed());
            firstBedroom.AddComponent(new Chair());
            firstBedroom.AddComponent(new Carpet());

            var secondBedroom = new Composite("Second Bedroom");
            secondBedroom.AddComponent(new Bed());
            secondBedroom.AddComponent(new Chair());
            secondBedroom.AddComponent(new Carpet());

            var bathRoom = new Composite("BathRoom");
            bathRoom.AddComponent(new Mirror());

            var room = new Composite("Apartments");
            room.AddComponent(firstBedroom);
            room.AddComponent(secondBedroom);
            room.AddComponent(new Chair());
            room.AddComponent(bathRoom);
            return room;
        }

        
    }
}
