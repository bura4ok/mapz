﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6.Patterns {

    public interface IListener {
        void Update(int data);
    }

    public class EventManager {
        private List<IListener> listeners = new List<IListener>();

        public void Subscribe(IListener listener) {
            listeners.Add(listener);    
        }
        public void Unsubscribe(IListener listener) {
            listeners.Remove(listener);  
        }

        public void Notify(int data) {
            foreach(var item in listeners) {
                item.Update(data);
            }
        }
    }
}
