﻿using Lab6.Patterns;

public interface IHotelState {
    void SetHotel(Hotel hotel);
    void Inited();
    void Furnished();
    string GetState();
}

class HotelInitialState : IHotelState {
    private Hotel _hotel;

    public void SetHotel(Hotel hotel) {
        _hotel = hotel;
    }

    public void Furnished() {
        _hotel.moneyBoost += 2.3;
        _hotel.TransitTo(new HotelFullyFurnishedState());
    }

    public void Inited() {
        return;
    }

    public string GetState() {
        return "Inited";
    }
}

public class HotelFullyFurnishedState : IHotelState {
    private Hotel _hotel;

    public void SetHotel(Hotel hotel) {
        _hotel = hotel;
    }

    public void Furnished() {
        return;
    }

    public void Inited() {
        _hotel.moneyBoost = 0;
        _hotel.TransitTo(new HotelInitialState());
        return;
    }



    public string GetState() {
        return "Furnished";
    }
}
    


class EmptyState : IHotelState {
    private Hotel _hotel;

    public void SetHotel(Hotel hotel) {
        _hotel = hotel;
    }

    public void Furnished() {
        return;
    }

    public void Inited() {
        _hotel.TransitTo(new HotelInitialState());
        return;
    }

    public string GetState() {
        return "Empty";
    } 
}