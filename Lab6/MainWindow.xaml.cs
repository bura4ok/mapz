﻿using Lab6.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            hotelFacade = new HotelFacade();
        }

        private HotelFacade hotelFacade;
        private Hotel h;

        private void btn_create1_Click(object sender, RoutedEventArgs e) {
            string hotelName = tb_name.Text;
            hotelFacade.CreateHotelSet1(hotelName);

            h = hotelFacade.Hotel;

            h.InitHotel();
            tb_state.Text = h.GetState;


            BindCommand(h);

            tb_main.Text = hotelFacade.ToString(); 
            var a = PlayerProfile.Profile;
            tb_rooms.Text = a.Furniture.ToString();
        }

        private void BindCommand(Hotel h) {
            btn_clear.Tag = new ClearHotelCommand(h);
            btn_save.Tag = new SaveHotelCommand(h);
        }

        private void btn_create2_Click(object sender, RoutedEventArgs e) {
            string hotelName = tb_name.Text;

            hotelFacade.CreateHotelSet2(hotelName);

            h.InitHotel();
            tb_state.Text = h.GetState;


            BindCommand(h);

            tb_main.Text = hotelFacade.ToString();

            var a = PlayerProfile.Profile;

            tb_rooms.Text = a.Furniture.ToString();
        }

        private void btn_perform_command_Click(object sender, RoutedEventArgs e) {
            var btn = sender as Button;
            (btn.Tag as Lab6.Patterns.ICommand).Execute();
            tb_main.Text = hotelFacade.ToString();
            
        }

        private void btn_furniture_Click(object sender, RoutedEventArgs e) {
            try {
                if (h._state is HotelInitialState) {
                    hotelFacade.CreateFurnitureTest();
                    h.FurnishedHotel();
                    tb_state.Text = h.GetState;

                }
            }
            catch (Exception zxc) {
                tb_main.Text = hotelFacade.ToString() + "\n";
                tb_main.Text += zxc.Message;

            }

            tb_main.Text = hotelFacade.ToString();
        }
    }



}







