using Lab3;
using NUnit.Framework.Constraints;
using System;
using System.Linq;

namespace UnitTesting {
    public class Tests {

        private TestModel model;
        
        [SetUp]
        public void SetUp() {
            model = new TestModel();
        }

        //1. �������� ������� ���������� (����� Select)
        [Test]
        public void TestSelect() {
            var result = model.hotels.Select(obj => obj.name.Length);

            Assert.That(model.hotels.ElementAt(0).name.Length, Is.EqualTo(result.ElementAt(0)));

        }

        //2. ������ ����� ���������� (Where)
        [Test]
        public void TestWhere() {
            var result = model.hotels.Where(obj => obj.rating.starRating > 2);

            Assert.That(result.Count(), Is.EqualTo(3));

        }



        //3. �������� �� � ������� List, ��� � � ��������� Dictionary
        [Test]
        public void TestOrderBtDescending() {
            var result = model.hotels.OrderByDescending(h => h.rating.starRating).ToList();

            Assert.That(model.hotels.ElementAt(0).name, Is.EqualTo("Almanity Hoi An Resort & Spa"));

            
        }

        [Test]
        public void TestDictionaryContainsKey() {
            Dictionary<Guid, List<Hotel>> result = new Dictionary<Guid, List<Hotel>>();

            foreach (var hotel in model.hotels) {
                if (!result.ContainsKey(hotel.rating.id)) { 
                    result[hotel.rating.id] = new List<Hotel>();
                }
                result[hotel.rating.id].Add(hotel); 
            }

            Assert.That(result.Keys, Is.All.TypeOf<Guid>());
        }

        //4. ������ ������ ������������
        [Test]
        public void TestExtensionMethod() {
            Hotel h = model.hotels[2];
            h.DropRating();

            Assert.That(h.rating.starRating, Is.EqualTo(1.0));
        }

        //5. ������������ ��������� ����� �� �������������
        [Test]
        public void TestAnonymousClasses() {
            var result = model.hotels.Select(hotel => new {
                Name = hotel.name,
                Stars = hotel.rating.starRating
            });
                
            Assert.AreEqual(result.ElementAt(0).Name, model.hotels[0].name);
            Assert.AreEqual(result.ElementAt(0).Stars, model.hotels[0].rating.starRating);
        }

        //6. ���������� �� ������ ������� �������������� ������ IComparer
        [Test]
        public void TestSorting() {
            var hotels = model.hotels;

            var a = hotels.OrderBy(h => h, new HotelComparer());//� �� �����
            //hotels.Sort(new  HotelComparer());

            var expectedNames = new List<string> { "Almanity Hoi An Resort & Spa", "Bulgari Bali", "Ctlantis the Royale", "Dotel Lviv", "Fairmont Ammant" };

            var actualNames = a.Select(h => h.name);

            Assert.AreEqual(expectedNames, actualNames);

        }

        //7. ������������� ������ � �����
        [Test]
        public void TestToArray() {
            Hotel[] arr = model.hotels.ToArray();

            for (int i = 0; i < model.hotels.Count(); i++) {
                Assert.AreEqual(model.hotels[i], arr[i]);
            }
        }

        //8. ���������� ������/������ �� ���� �� �� ������� ��������
        [Test]
        public void TestSort2() {
            var result = model.hotels;

            result.Sort((x, y) => string.Compare(x.name, y.name));


            Assert.That(result[1].name, Is.EqualTo("Bulgari Bali"));

            //Assert.AreEqual(result[0], model.hotels[0]);
        }

        //��������� � ��������� ��������� Dictionary, � ��� �� ������ ����� ��������� ������� ����, ��� �� ������
        [Test]
        public void TestToDictionary() {
            var result = model.hotels.ToDictionary(
                hotel => hotel.id,
                hotel => hotel
                );

            Assert.That(result.Keys, Is.All.TypeOf<Guid>());
        }

        //6) �������� � ������ ���������� �����������: SortedList, Queue
        [Test]
        public void TestQueue() {
            var result = new Queue<Hotel>(model.hotels);
            Assert.That(result.Peek().name, Is.EqualTo("Almanity Hoi An Resort & Spa"));
        }

        [Test]
        public void TestStack() {
            var result = new Stack<Hotel>(model.hotels);
            result.Pop();

            Assert.That(result.Peek().name, Is.EqualTo("Dotel Lviv"));
        }


        [Test]
        public void TestHashSet() {
            var result = new HashSet<Hotel>(model.hotels);

            Hotel h = result.First();


            Assert.That(5, Is.EqualTo(result.Count));
            Assert.IsTrue(result.Contains(h));
        }


        //7) ���������� ��� ���� ��������� ��������. 
        [Test]
        public void TestGroupBy() { 
            var result = model.hotels.GroupBy( hotel => hotel.rating.starRating);

            var numberOf5 = result.FirstOrDefault(group => group.Key == 5);

            int count = numberOf5.Count();

            Assert.That(2, Is.EqualTo(count));

            List<Hotel> lh = model.hotels;
            Hotel h = new Hotel("New", 2.0);


            lh.AddToFront(h);

            Assert.That(h, Is.EqualTo(lh.First()));

        }


        //8) ����������� ��������� �������� �� ����������� �� �����������.
        [Test]
        public void TestComplicated() {
            List<List<Hotel>> llh = new List<List<Hotel>>();

            llh.Add(new List<Hotel> {
                new Hotel("A", new HotelRatingStars(2.0).starRating),
                new Hotel("AA", new HotelRatingStars(3.0).starRating),
                new Hotel("AAA", new HotelRatingStars(4.0).starRating) });
            llh.Add(new List<Hotel> { 
                new Hotel("B", new HotelRatingStars(1.0).starRating),
                new Hotel("BB", new HotelRatingStars(4.0).starRating) });
            llh.Add(new List<Hotel> { 
                new Hotel("C", new HotelRatingStars(1.0).starRating), 
                new Hotel("CC", new HotelRatingStars(2.0).starRating), 
                new Hotel("CCC", new HotelRatingStars(2.0).starRating),
                new Hotel("CCCC", new HotelRatingStars(5.0).starRating) });

            var result = llh.Where(list => list
                .Any(hotel => hotel.rating.starRating == 1))
                .OrderBy(list => list.Count());

            //Assert.AreEqual(2, result.Count());

            Assert.That(2, Is.EqualTo(result.Count()));
        }
    }
}

