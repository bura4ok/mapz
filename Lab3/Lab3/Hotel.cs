﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3 {
    public class Hotel {
        public string name;

        public Guid id;

        public HotelRatingStars rating;

        public Hotel(string name, double rating) {
            this.name = name;
            this.id = Guid.NewGuid();
            this.rating = new HotelRatingStars(rating);
        }

        public override string ToString() {
            return String.Format("Hotel name: {0} with {1} stars. ID = {2}", name, rating.starRating, id);
        }
    }

    public class HotelRatingStars {
        public Guid id = new Guid();
        public double starRating;

        public HotelRatingStars(double rating) {
            starRating = rating;
        }
    }

    public class TestModel {
        public List<Hotel> hotels;
        public List<HotelRatingStars> ratings;

        public TestModel() {
            ratings = new List<HotelRatingStars>() {
                new HotelRatingStars(1.0),
                new HotelRatingStars(2.0),
                new HotelRatingStars(3.0),
                new HotelRatingStars(4.0),
                new HotelRatingStars(5.0)
            };


            hotels = new List<Hotel>();

            hotels.Add(new Hotel("Almanity Hoi An Resort & Spa", ratings[4].starRating));
            hotels.Add(new Hotel("Ctlantis the Royale", ratings[2].starRating));
            hotels.Add(new Hotel("Fairmont Ammant", ratings[1].starRating));
            hotels.Add(new Hotel("Dotel Lviv", ratings[0].starRating));
            hotels.Add(new Hotel("Bulgari Bali", ratings[4].starRating));


        }
    }

    public static class ExtensionMethod {
        public static void DropRating(this Hotel hotel) {
            if (hotel == null) return;
            hotel.rating.starRating = 1.0;
        }

        public static void AddToFront<T>(this List<T> list, T item) {
            list.Insert(0, item);
        }
    }

    public class HotelComparer : IComparer<Hotel> {
        public int Compare(Hotel? x, Hotel? y) {

            if (x == null || y == null) return 0;

            return String.Compare(x.name, y.name);
        }
    }

    
}

