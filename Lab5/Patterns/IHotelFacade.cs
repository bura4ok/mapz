﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Patterns {
    public interface IHotelFacade {
        void AddTableToAll();
        void AddFurnitureToAll(IComponent item);
        void CreateHotelSet1(string name);
        void CreateHotelSet2(string name);
        void CreateFurnitureTest();
    }
}
