﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Patterns {
    public interface IComponent {
        string Clear(int d);
        string Print(int d);
    }

    public class Bed : IComponent {
        public string Name => "Bed";
        public string Clear(int d) {
            return new string('*', d) + $"Cleaning {Name}";
        }

        public string Print(int d) {
            return new string('*', d) + Name;
        }
    }

    public class Mirror : IComponent {
        public string Name => "Mirror";
        public string Clear(int d) {
            return new string('*', d) + $"Cleaning {Name}";
        }
        public string Print(int d) {
            return new string('*', d) + Name;
        }
    }

    public class Chair : IComponent {
        public string Name => "Chair";
        public string Clear(int d) {
            return new string('*', d) + $"Cleaning {Name}";
        }
        public string Print(int d) {
            return new string('*', d) + Name;
        }
    }

    public class Carpet : IComponent {
        public string Name => "Carpet";
        public string Clear(int d) {
            return new string('*', d) + $"Cleaning {Name}";
        }
        public string Print(int d) {
            return new string('*', d) + Name;
        }
    }

    class Composite : IComponent {
        public string Name => _name;
        string _name;
        private List<IComponent> components = new();

        public Composite(string name) {
            _name = name;
        }

        public Composite() {
            _name = String.Empty;
        }

        public void AddComponent(IComponent component) {
            components.Add(component);
        }

        public string Clear(int d = 0) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(new string('*', d) + $"Cleaning {Name}...");
            foreach (IComponent component in components) {
                sb.AppendLine(component.Clear(d + 1));
            }
            return sb.ToString();
        }

        public string Print(int d = 0) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(new string('*', d) + Name);
            foreach (IComponent component in components) {
                sb.AppendLine(component.Print(d + 1));
            }
            return sb.ToString();
        }

        public override string ToString() {
            return Print(0);
        }
    }
}
