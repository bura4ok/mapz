﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Patterns {
    public class HotelProxy : IHotelFacade {
        private HotelFacade _hotelFacade;
        private PlayerProfile _playerProfile;


        public Hotel Hotel {
            get { return _hotelFacade.Hotel; }
        }

        public HotelProxy() {
            _hotelFacade = new HotelFacade();
            _playerProfile = PlayerProfile.Profile;
        }

        public void AddFurnitureToAll(IComponent item) {
            if (_playerProfile.State == PlayerState.Owner
                && _playerProfile.Level > 5
                && _hotelFacade.Hotel != null) {
                _hotelFacade.AddFurnitureToAll(item);
            } else {
                throw new Exception("Errors \nLow level\nWrong mode");
            }
        }

        public void AddTableToAll() {
            if (_playerProfile.State == PlayerState.Owner
                && _playerProfile.Level > 5
                && _hotelFacade.Hotel != null) {
                _hotelFacade.AddTableToAll();
            } else {
                throw new Exception("Errors \nLow level\nWrong mode");

            }
        }

        public void CreateHotelSet1(string name) {
            if (_playerProfile.State == PlayerState.Owner
                && _hotelFacade.Hotel == null
                && name != String.Empty) {
                _hotelFacade.CreateHotelSet1(name);
            } else {
                throw new Exception("Errors \nWrong Mode\nEmpty Name");
            }
        }

        public void CreateHotelSet2(string name) {
            if (_playerProfile.State == PlayerState.Owner
                && _hotelFacade.Hotel == null
                && name != String.Empty) {
                _hotelFacade.CreateHotelSet2(name);
            }
            else {
                throw new Exception("Errors \nWrong Mode\nEmpty Name");
            }
        }

        public override string ToString() {
            if (_hotelFacade.Hotel != null) {
                return _hotelFacade.ToString();
            }
            return "No Hotel";
        }

        public void CreateFurnitureTest() {
            if (_playerProfile.State == PlayerState.Owner
                && _hotelFacade.Hotel != null) {
                _hotelFacade.CreateFurnitureTest();
            } else {
                throw new Exception("Errors \nWrong Mode");
            }
        }
    }
}
