﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Lab5.Patterns {
//    public class Component {
//        private string _name;

//        public virtual string Name => _name;

//        public virtual bool IsComponent() {
//            return true;
//        }

//        public Component() {
//            _name = "Unknown Group";
//        }

//        public Component(string name) {
//            _name = name;
//        }

//        private List<Component> _children = new List<Component>();

//        public override string ToString() {
//            StringBuilder sb = new StringBuilder();
//            Print(sb, 0);
//            return sb.ToString();
//        }

//        private void Print(StringBuilder sb, int d) {
//            sb.Append(new string('*', d))
//                .AppendLine(Name);
//            foreach (var items in _children) {
//                items.Print(sb, d + 1);
//            }
//        }

//        public void Add(Component item) {
//            if (IsComponent()) {
//                _children.Add(item);
//            }
//            else {
//                Console.WriteLine("wefewfedw");
//            }
//        }

//        public void Remove(Component item) {
//            if (IsComponent()) {
//                _children.Remove(item);
//            }
//        }
//    }

//    public class Chair : Component {
//        public override string Name => "Chair";
//        public override bool IsComponent() {
//            return false;
//        }
//    }

//    public class Carpet : Component {
//        public override string Name => "Carpet";
//        public override bool IsComponent() {
//            return false;
//        }
//    }

//    public class Mirror : Component {
//        public override string Name => "Mirror";
//        public override bool IsComponent() {
//            return false;
//        }
//    }


//    public class Bed : Component {
//        public override string Name => "Bed";
//        public override bool IsComponent() {
//            return false;
//        }
//    }
//}
