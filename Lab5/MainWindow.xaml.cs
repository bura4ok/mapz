﻿using Lab5.Patterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private HotelProxy hotel = new HotelProxy();

       
        private void btn_mode_Click(object sender, RoutedEventArgs e) {
            var pr = PlayerProfile.Profile;

            if(pr.State == PlayerState.Owner) {
                pr.ChangeState();
                tb_mode.Text = "Current mode: Spectator";
            } else if (pr.State == PlayerState.Spectator) {
                pr.ChangeState();
                tb_mode.Text = "Current mode: Owner";
            }
        }

        private void btn_set1_Click(object sender, RoutedEventArgs e) {
            string hotelName = tb_name.Text;
            try {
                hotel.CreateHotelSet1(hotelName);
            }
            catch (Exception zxc) {
                tb_main.Text += zxc.Message;

            }
            tb_main.Text = hotel.ToString() + "\n";

        }

        private void btn_set2_Click(object sender, RoutedEventArgs e) {
            string hotelName = tb_name.Text;
            try {
                hotel.CreateHotelSet2(hotelName);
            }
            catch (Exception zxc) {
                tb_main.Text += zxc.Message;
            }
            tb_main.Text = hotel.ToString() + "\n";


        }

        private void btn_test_set_Click(object sender, RoutedEventArgs e) { 
            try {
                hotel.CreateFurnitureTest();
            }
            catch (Exception zxc) {
                tb_main.Text = hotel.ToString() + "\n";
                tb_main.Text += zxc.Message;

            }

            tb_main.Text = hotel.ToString();
        }

        private void btn_add_Click(object sender, RoutedEventArgs e) {
            
            string type = cb_type.Text;


            try {
                IComponent item = ParseComponent(type);
                hotel.AddFurnitureToAll(item);
            }
            catch (Exception zxc) {
                tb_main.Text = zxc.Message;
                return;
            }

            tb_main.Text = hotel.ToString();
        }

        private IComponent ParseComponent(string type) {
            switch (type.ToLower()) {
                case "chair":
                    return new Chair();
                case "bed":
                    return new Bed();
                case "mirror":
                    return new Mirror();
                case "carpet":
                    return new Carpet();
                default:
                    throw new ArgumentException();
            }
        }

        private void tb_lvl_TextChanged(object sender, TextChangedEventArgs e) {
            var profile = PlayerProfile.Profile;
            if (tb_lvl.Text == String.Empty) {
                return;
            }
            profile.Level = Convert.ToInt32(tb_lvl.Text);
        }
    }
}
